AUTHOR: Paul Feuvraux

# Introduction

As we cannot do search on encrypted content, and as 
the titles will be end-to-end encrypted, we won't be able to 
offer the search feature. But we can still do it locally.

# Idea

The idea is the following: we use the embedded SQLite of Android 
and iOS, and use it as an indexation database in order to allow search 
within the mobile application.

The SQLite database will be synced with the metadata locally stored in the 
file system. The FS will be synced with the cloud anyway, and vice versa.

# Privacy issues

On SQLite, the titles won't be encrypted, they'll be stored as plaintext. 
Hence, we cannot offer this feature by default, but it will be a feature 
which could be enabled or disabled on the fly.

# Enable / Disable

At first, when enabling this feature, the metadata of the FS will be populating 
the SQLite database. When disabling this feature, the SQLite database will be 
dropped. Which means that all the files' titles will be decrypted and then will be 
populating the SQLite database.

# On the fly indexation

Any change within the path of the locally stored files (on the device) will be 
updating the SQLite database.

# Conclusion

We'll be able to offer a search feature which will cover all the directories, without 
violating users' privacy (as it will be offered as an option). We'll also be able to make less 
requests to the API for discovering files and folders.