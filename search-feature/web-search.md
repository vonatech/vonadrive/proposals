AUTHOR: Paul Feuvraux

Status: :fire: | Not approved

# Introduction

As we cannot do search on encrypted content, and as 
the titles will be end-to-end encrypted, we won't be able to 
offer the search feature. But we can still do it locally.

# Idea

We'll use ElasticSearch in order to make the files and folders 
searchable. The search feature on web will be an option, 
in order to avoid violating users' privacy.

# Enabling / Disabling

- when enabling, all the relevant metadata (TBD) will be indexed 
into ES
- when disabling, all the indexed metadata will be discarded.

# On-the-Fly indexation

When this feature is enabled, the metadata are indexed into ES 
in the same time as CQL, in parallel.

## Titles encryption

Titles will remain encrypted into the CQL database, but will be stored in 
plaintext into ES. Which means that when the web-search feature will be 
enabled, the title will be duplicated: the e2e encrypted one, and the plaintext 
one.

# Final note

Couldn't get it to work, as the titles will be encrypted since the very 
beginning by default, this feature will only be available for the files uploaded 
only after the feature gets enabled, which is irrelevant.