AUTHOR: Paul Feuvraux

# Introduction

We've worked on a proposal to sign off new devices authentications, 
an issue would have happened in the case no device is logged in with 
the user's account. Hence, we plan to make a cloud-based PIN, which 
will allow the user to safely and quickly log in into their account even 
though no devices are logged in.

# Concept

The user will have to define a PIN, which will be stored within the database. 
For security issue, this PIN will be hashed via one way or the other.

This feature will be only available when no devices are logged in with 
the user's account.

# Conclusion

This will be the simpliest auth verification method for users who chose the 
domain-auth-verification method.