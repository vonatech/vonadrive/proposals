#!/usr/bin/env node
const fs = require('fs')
const ini = require('ini')
const chalk = require('chalk')
const util = require('util')
const cassandra = require('cassandra-driver')

let config = ini.parse(fs.readFileSync('./conf.ini', 'utf-8'))

let nb_rows = parseInt(config.tests.rows)
let nb_points = parseInt(config.tests.points)

let every = Math.floor(nb_rows / nb_points)
console.log(chalk.magenta('Scylla - 1M reads'))

const run = async () => {
  let options = {
    keyspace: config.database.keyspace,
    contactPoints: typeof config.database.host === 'string' ? [config.database.host] : config.database.host
  }
  if (typeof config.database.user !== 'undefined' && typeof config.database.password) {
    options.authProvider = new PlainTextAuthProvider(config.database.user, config.database.password)
  }
  if (typeof config.database.ssl !== 'undefined') {
    switch (config.database.ssl) {
      case 'basic':
        options.sslOptions = {}
        break
      case 'certificate':
        options.sslOptions = {ca: [config.database.server_cert]}
        break
      case 'client_certificate':
        options.sslOptions = {ca: [config.database.server_cert], key: config.database.private_key, cert: config.database.client_cert}
        break
    }
  }
  const cql = new cassandra.Client(options)
  cql.connect()

  console.log(chalk.green('Connected to Scylla database'))

  let stream = fs.createWriteStream('results.csv')
  stream.once('open', () => {
    console.log(chalk.green('Reading...'))
    let query = 'SELECT file_id FROM file_by_id LIMIT 1'
    let total = Date.now()
    let point = Date.now()
    let before_insert, speed

    let read = (i) => {
      cql.execute(query, [], {}, () => {
        if (every > 0 && (i + 1) % every === 0) {
          // Insert a point
          before_insert = Date.now()
          speed = (before_insert - point) > 0 ? Math.round(((every / (before_insert - point)).toFixed(3) * 1000)) : 0 // nb_writes/s
          console.log(chalk.yellow('Current speed: ' + speed + ' reads/s'))
          stream.write(speed + '\n')

          point = Date.now()
          total += (point - before_insert)
        }

        if ((i + 1) < nb_rows) {
          read(i + 1)
        } else {
          stream.end()
          console.log(chalk.green('Done. Total for ' + nb_rows + ' rows: ' + (Date.now() - total) + ' ms'))
          process.exit(0)
        }
      })
    }
    read(0)
  })
}

run()
