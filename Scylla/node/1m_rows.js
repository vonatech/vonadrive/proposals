#!/usr/bin/env node
const fs = require('fs')
const ini = require('ini')
const chalk = require('chalk')
const util = require('util')
const cassandra = require('cassandra-driver')

let config = ini.parse(fs.readFileSync('./conf.ini', 'utf-8'))

let nb_rows = parseInt(config.tests.rows)
let nb_points = parseInt(config.tests.points)

let every = Math.floor(nb_rows / nb_points)
console.log(chalk.magenta('Scylla - 1M rows'))

const run = async (clear = false) => {
  let options = {
    keyspace: config.database.keyspace,
    contactPoints: typeof config.database.host === 'string' ? [config.database.host] : config.database.host
  }
  if (typeof config.database.user !== 'undefined' && typeof config.database.password) {
    options.authProvider = new PlainTextAuthProvider(config.database.user, config.database.password)
  }
  if (typeof config.database.ssl !== 'undefined') {
    switch (config.database.ssl) {
      case 'basic':
        options.sslOptions = {}
        break
      case 'certificate':
        options.sslOptions = {ca: [config.database.server_cert]}
        break
      case 'client_certificate':
        options.sslOptions = {ca: [config.database.server_cert], key: config.database.private_key, cert: config.database.client_cert}
        break
    }
  }
  const cql = new cassandra.Client(options)
  cql.connect()

  console.log(chalk.green('Connected to Scylla database'))
  if (clear) {
    console.log(chalk.white('Clearing table'))
    cql.execute('TRUNCATE file_by_id', () => {
      console.log(chalk.green('Cleared table'))
      run()
    })
    return true
  }

  let stream = fs.createWriteStream('results.csv')
  stream.once('open', () => {
    console.log(chalk.green('Inserting rows...'))
    let query = `INSERT INTO file_by_id (file_id, owner_id, folder_id, updated_at, name, size, path, tree, flags, dk, shared_with, lek)
     VALUES (uuid(), uuid(), uuid(), ?, 'test', 0, '', [], {}, '', {}, {})`

    let total = Date.now()
    let point = Date.now()
    let before_insert, speed

    let insert = (i) => {
      cql.execute(query, [Date.now()], {prepare: true}, () => {
        if (every > 0 && (i + 1) % every === 0) {
          // Insert a point
          before_insert = Date.now()
          speed = (before_insert - point) > 0 ? Math.round(((every / (before_insert - point)).toFixed(3) * 1000)) : 0 // nb_writes/s
          console.log(chalk.yellow('Current speed: ' + speed + ' writes/s'))
          stream.write(speed + '\n')

          point = Date.now()
          total += (point - before_insert)
        }

        if ((i + 1) < nb_rows) {
          insert(i + 1)
        } else {
          stream.end()
          console.log(chalk.green('Done. Total for ' + nb_rows + ' rows: ' + (Date.now() - total) + ' ms'))
          process.exit(0)
        }
      })
    }
    insert(0)
  })
}

run(process.argv.indexOf('clear') !== -1 || process.argv.indexOf('--clear') !== -1)
