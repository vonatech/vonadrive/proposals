#!/usr/bin/env node
const fs = require('fs')
const ini = require('ini')
const chalk = require('chalk')
const util = require('util')
const cassandra = require('cassandra-driver')

let config = ini.parse(fs.readFileSync('./conf.ini', 'utf-8'))
console.log(chalk.magenta('Scylla - 1M reads'))

const run = async () => {
  let options = {
    keyspace: config.database.keyspace,
    contactPoints: typeof config.database.host === 'string' ? [config.database.host] : config.database.host
  }
  if (typeof config.database.user !== 'undefined' && typeof config.database.password) {
    options.authProvider = new PlainTextAuthProvider(config.database.user, config.database.password)
  }
  if (typeof config.database.ssl !== 'undefined') {
    switch (config.database.ssl) {
      case 'basic':
        options.sslOptions = {}
        break
      case 'certificate':
        options.sslOptions = {ca: [config.database.server_cert]}
        break
      case 'client_certificate':
        options.sslOptions = {ca: [config.database.server_cert], key: config.database.private_key, cert: config.database.client_cert}
        break
    }
  }
  const cql = new cassandra.Client(options)
  cql.connect()

  console.log(chalk.green('Connected to Scylla database'))

  let stream = fs.createWriteStream('results.csv')
  stream.once('open', () => {
    console.log(chalk.green('Reading...'))
    let query = 'SELECT file_id FROM file_by_id'
    let a = Date.now()
    let b = Date.now()

    // Automatic paging
    /*
    cql.stream(query, [], {fetchSize: 1000}).on('readable', function () {
      // emitted as soon a row is received
      let row
      // this.buffer.length
      while (row = this.read()) {
        // process row
      }
      console.log(chalk.green('Retrieved x rows in ' + (Date.now() - b)))
    }).on('end', () => {
      // emitted when all rows have been retrieved and read
      console.log(chalk.green('Total read: ' + (Date.now() - b)))
      process.exit(0)
    }).on('error', () => {
      console.log(chalk.red('Something went wrong'))
      process.exit(0)
    })
    */

    let last = 0
    let speed
    cql.eachRow(query, [], {fetchSize: 1000}, () => {}, (err, result) => {
      speed = Math.round(((result.rowLength - last) / (Date.now() - a)).toFixed(3) * 1000) // nb_reads/s
      console.log(chalk.white('Current speed: ' + speed + ' reads/s'))
      stream.write(speed + '\n')
      last = result.rowLength
      a = Date.now()
      if (result.nextPage) {
        result.nextPage()
      } else {
        stream.end()
        console.log(chalk.green('Total read for ' + result.rowLength + ' rows: ' + (Date.now() - b) + ' ms'))
        process.exit(0)
      }
    })
  })
}

run()
