<?php
set_time_limit(0);

const host = "127.0.0.1";
const keyspace = "cloud";
const ssl = "none";

$nb_rows = 1000000;
$nb_points = 1000;

$every = floor($nb_rows / $nb_points);

$fp = fopen('results.csv', 'w');

$cql = \Cassandra::cluster()
  ->withContactPoints(host)
  ->build()
  ->connect(keyspace);

$read = "SELECT file_id FROM file_by_id LIMIT 1";

$q = $cql->prepare($read);
$total = microtime(true);
$point = microtime(true);
for ($i = 0; $i < $nb_rows; $i++) {
  $cql->execute($q);
  if ($every > 0 && ($i + 1) % $every === 0) {
    // Insert a point
    $before_insert = microtime(true);
    $speed = ($before_insert - $point) > 0 ? round($every / ($before_insert - $point)) : 0; // nb_writes/s
    fputcsv($fp, [$speed]);

    $point = microtime(true);
    $total += ($point - $before_insert);
  }
}
echo 'Total for '.$nb_rows.' rows: '.(microtime(true) - $total).' s';
?>
