<?php
set_time_limit(0);

const host = "192.168.1.54";
const keyspace = "cloud";
const ssl = "none";

$truncate = true;

$nb_rows = 1000000;
$nb_points = 1000;

////////////////////////////

$every = floor($nb_rows / $nb_points);

$fp = fopen('results.csv', 'w');

$cql = \Cassandra::cluster()
  ->withContactPoints(host)
  ->build()
  ->connect(keyspace);

if ($truncate) {
  $cql->execute("TRUNCATE file_by_id");
}

$insert = "INSERT INTO file_by_id (file_id, owner_id, folder_id, updated_at, name, size, path, tree, flags, dk, shared_with, lek)
 VALUES (uuid(), uuid(), uuid(), :time, 'test', 0, '', [], {}, '', {}, {})";

$time = new \Cassandra\Timestamp(time());

$q = $cql->prepare($insert);
$total = microtime(true);
$point = microtime(true);
for ($i = 0; $i < $nb_rows; $i++) {
  $cql->execute($q, [
    'arguments' => ['time' => $time]
  ]);
  if ($every > 0 && ($i + 1) % $every === 0) {
    // Insert a point
    $before_insert = microtime(true);
    $speed = ($before_insert - $point) > 0 ? round($every / ($before_insert - $point)) : 0; // nb_writes/s
    fputcsv($fp, [$speed]);

    $point = microtime(true);
    $total += ($point - $before_insert); // do not count insertion
  }
}
echo 'Total for '.$nb_rows.' rows: '.(microtime(true) - $total).' s';
?>
