<?php
set_time_limit(0);

const host = "127.0.0.1";
const keyspace = "cloud";
const ssl = "none";

$nb_rows = 1000000;
$page_size = 1000;

$fp = fopen('results.csv', 'w');

$cql = \Cassandra::cluster()
  ->withContactPoints(host)
  ->build()
  ->connect(keyspace);

$query = "SELECT file_id FROM file_by_id";
$page_token = null;

$b = microtime(true);
do {
  $a = microtime(true);
  $l = '';
  $read = $cql->execute($query, array_merge(['page_size' => $page_size], isset($page_token) ? ['paging_state_token' => $page_token] : []));
  $page_token = $read->pagingStateToken();
  if ($page_token === null) break;
  $speed = round(($read->count()) / (microtime(true) - $a));
  fputcsv($fp, [$speed]);
  echo 'Current speed: '.$speed.' reads/s<br>';
} while ($read->pagingStateToken());
echo '<br>Total read: '.(microtime(true) - $b);
?>
