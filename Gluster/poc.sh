#!/usr/bin/env bash

RED='\033[0;31m'
NC='\033[0m'

function _help(){
  echo "\
--read [filelim] [id] [:verbose -v]
--write [file limit] [id] [path] [blocksize] [:verbose -v]
--is-reading [path]"
exit 0
}

function _read(){
  local counter=0
  local filelim=$1
  local p=0
  local id=$2
  read -p "Enter the path (must be absolute): " p

  if [[ -z $id ]]; then
    read -p "Enter the id: " id
  fi

  local f=$(ls $p|grep "$id-"|wc -l)
  if [[ $f -ge $filelim ]]; then
    local filelim=$f
  fi

  if [[ $3 == "-v" ]]; then
    local o="stdout"
  else
    local o="null"
  fi

  echo "Processing..."
  while [[ $counter -lt $filelim ]]; do
    if [[ $3 == "-v" ]]; then
      echo "Reading $p/$id-$counter"
    fi
    curl file://$p/$id-$counter &>/dev/null
    counter=$(( counter + 1 ))
  done
}

function isAbleRead(){
  local c=0
  local path=$1
  while true;do
    local x=$(ls $path)
    if [[ ! -z $x ]]; then
      echo -ne "\007"
      if [[ $c -ne 0 ]]; then
        echo "I see something!"
      fi
    else
      if [[ $c -ne 0 ]]; then
        echo "I don't see anything!"
      fi
    fi
    c=$(( c + 1 ))
    sleep 1s;
    if [[ $c -ge 4 ]]; then
      clear
      c=0
    fi
  done
}

function _write(){
  local counter=0
  local filelim=$1
  local id=$2
  local p=$3
  local blocksize=$4
  local z=0

  if [[ -z $filelim ]]; then
    read -p "Enter the limit of files to be written: " filelim
  fi
  if [[ -z $id ]]; then
    read -p "Enter the id of the files for this client: " id
  fi
  if [[ -z $blocksize ]]; then
    read -p "Enter blocksize: " blocksize
  fi
  if [[ -z $p ]]; then
    read -p "Enter the path to write to: " p
  fi
  if [[ "$5" == "-v" ]]; then
    q="stdout"
  else
    q="null"
  fi
  echo "Processing..."
  while [[ $counter -lt $filelim ]]; do
    local t=$SECONDS
    z=$(( RANDOM % $blocksize ))
    if [[ $z -eq 0 ]]; then
      z=$(( RANDOM % $blocksize ))
    fi
    dd if=/dev/urandom of=${p}/${id}-$counter bs=${z}MB count=1 iflag=fullblock &>/dev/${q}
    counter=$(( counter + 1 ))
    if [[ ! -z $5 ]]; then
      echo -ne "\n${RED}Written ${z}MB in $(( SECONDS - t))${NC}\n"
    fi
  done
}

k=$1

case $k in
  "--read"|"-r")
    shift
    time _read $@
    echo "Done"
    ;;
  "--write"|"-w")
    shift
    time _write $@
    echo "Done."
    ;;
  "--is-reading")
    shift
    isAbleRead $@
    ;;
  "--help"|"-h") _help ;;
  *) _help ;;
esac
exit 0
