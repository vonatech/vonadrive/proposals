AUTHOR: Paul Feuvraux

# Introduction

Our current auth verification systems are using 
emails or google authenticator. We aim to remain 
within our own domain to sign off new devices 
authentication.

# Concept

First of all, the signed in user goes through the 
settings page, and allow "muonium authentication". 
Nothing more will be required to do regarding the activation 
of this auth verification method.

Considering this auth verification method enabled, the user will 
try to log in with a new device (mobile, computer, browser, satellite...). 
A notification on all the other devices will be pushed, and will ask the 
user if the new device auth was their doing. The user will then be able to 
either accept this new device, or refuse it.

# Tweaks

Here's a list of the available tweaks that we would implement (for advanced users):

- time-based: the user define the time range, if the user doesn't click on "accept" during 
this range, then the new device doesn't get logged in and a new authentication is required. 
*Note*: it will be time based anyway, max time for allowing a new device will be 10m (or TBD), 
but the user will be able to decide their own range between 5s and 10min.
- several devices: the user needs to accept from several devices. For instance, the user needs 
to accept from their mobile and their computer (desktop app / web app).
- PIN-based: when the user accepts a request, they define a single-use PIN only. The user will have 
to enter this PIN onto the new device in order to have it authenticated.

Those parameters will actually be independent, which means that 
all of them can be enabled.

# Backup solution

When the user logs out from all their devices, this method is no longer available, 
to counter this issue. The user would have to pick one of those methods:

- sending a code via email
- google auth
- cloud-PIN (a cloud-based PIN, not implemented yet, see the proposal)

# Conclusion

This new auth verification method will allow us not to depend on any third party, 
and will allow us to use multiple parameters at once in order to increase the 
security degree.