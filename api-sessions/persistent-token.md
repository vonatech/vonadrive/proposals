AUTHOR: Paul Feuvraux

# Introduction

Currently, tokens only last for a few minutes, and get the 
web application disconnected when out of the time range. 
We want to develop mobile & desktop applications, which 
will require persistent tokens.

# System

Here's a few requirements:

- the token needs to be valid for one month
- for security reasons, every 10 min, the mobile / desktop apps 
will have their tokens changed.

So, a persistent token will be valid for one month in the case it is 
not used for that period. But, in order to renew it as much as possible 
for more security, for every request made by a mobile or desktop device, 
if the token is older than 10min, it'll get changed. In the case the 
token is older than 1 month, then the device gets logged out.

# Cron

A CRON will be run periodically, and will delete every persistent token older 
than 1 month.

# Conclusion

Generally, mobile & desktop users would use the applications at least weekly. 
Hence, a 1 month-valid token will be way enough in order to keep their session 
open without having them to log themselves in again. Moreover, our applications will 
(our web app already has got it) a sessions manager, which allows to log out a device.