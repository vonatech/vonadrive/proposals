Author: Paul Feuvraux

*Virtual Folders, aka Circles*

# Introduction

Currently, our crypto-system doesn't allow us to share a
non-empty folder. We indeed plan to make it available,
through sharing the folder before any file gets uploaded
inside it. Although, an issue remains: users usually want to
share a folder when there already is something inside it.
In order to counter this bad UX experience, we plan to
implement _virtual folders_. We could indeed re-think
all our crypto-system, but this would make all our users to
download and re-upload all their files. Changing our current
structure would require us to design again our whole crypto-
system while designing _virtual folder_ would bring even more
features.

# Concept

A _virtual folder_ is a label put on the files that the user
wants to share as a cluster. A file may only be shared within
one unique _virtual folder_. Which means that a file cannot be
put within several _virtual folders_.

## Grouping files

A virtual folder has a `uuid`. Hence, a file shared through
a _virtual folder_ will be have the uuid of the _virtual folder_
that it has been shared through.

CQL scheme example:

```cql
CREATE TABLE file_by_id (
	file_id uuid,
	owner_id uuid,
	folder_id uuid,
	updated_at timestamp,
	name text,
	size bigint,
	path text,
	tree list<uuid>,
	flags set<text>,
	dk text,
	lek map<uuid, text>,
	dk_asym map<uuid, text>,
  circle uuid,
	PRIMARY KEY (file_id, owner_id)
);
```

On the scheme above, the file may have the `uuid` of the
_virtual folder_.

## Sharing the keys - Cryptography model

When a file is shared within a _virtual folder_, the client
gets the FEK of the file and then encrypts it under the CiEK
(Circle Encryption Key) which is randomly generated over 256 
bits. This key is encrypted under the users' PKB, and is used 
to encrypt the FEK of every file.

### Protocol - Between registered users

1. The user creates a circle - a CiEK is randomly generated over 256 bits
2. The user wants to share a file through this circle
3. The client downloads the first chunk of the server
4. The client recomputes the FEK of the file
5. The client encrypts the FEK under the CiEK
6. The client encrypts the CiEK under the PKB of the users that the user wants
to share with.

### Protocol - Externally shared

1. The user creates a circle - a CiEK is randomly generated over 256 bits
2. The user wants to share a file through this circle
3. The user defines a circle password in order to share the circle with external users (non-registered users)
4. The client downloads the first chunk of the file and recomputes the FEK of it
5. The client encrypts the FEK under the CiEK
6. The client derives the circle password (`CiKEK=PBKDF2(sha256, CP, s, 7000, 256)`),
where `s` (the salt) is a randomly generated 128-bit array.
7. The client encrypts the CiEK under the CiKEK.

### CQL Scheme

```cql
CREATE TABLE circle_by_id (
  circle_id uuid,
  files set<uuid>,
  asym_key map<uuid,text>,
  key text,
  PRIMARY KEY (circle_id)
  )
```

# UX

## "Circle"

Non-technical users don't like when it comes to use
"technical" terms such as "virtual". As it sounds very
programming-oriented, people will be more unwilling to use
this feature, or will not fully understand the concept.

Hence, in regards to having an accessible service for everyone,
using such a metaphorical term will bring more users to understand
and use this feature.

## Layout

We'll make a section called `Circles`, which will be next to the
`shared with me` section (for instance).

The user will click on "Add to circle" in the options of the file.
Then, the user will be able to either choose one already existing
circle, or create a new one. The user will also be able to share a
whole folder if they want to directly share a folder content and
mix this content with other files from another folder.

# Conclusion

Actually, making internally physical folders will bring us to re-design
all over the current structure of our crypto-system. A file in a physical
folder won't be able to be shared with other files which would be stored in
different physical folders. Although, thanks to the `Circles`, the user
will be allowed to shared files of different folders and make them available
with one key only. More flexibility, and efficiency will come in consequence
from this concept. The folder sharing model might actually be useless if we
consider how flexible, easy-to-use, and fast this concept is.

# Note

We're going to use Argon2 instead of PKBDF2.
