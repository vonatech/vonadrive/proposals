AUTHOR: Paul Feuvraux

# Introduction

We've done an analysis on the different 
performances of Cassandra/Scylla in comparison 
with SQL (MariaDB). We noticed that the READs of 
a CQL engine (Scylla/Cassandra) get to be slower 
and slower in following the amount of stored data. 
Hence, in order to improve our READs, we decided 
to explore ElasticSearch, which provides a search 
engine called Lucene.

# Issues

We've stumbled on several issues:

- ElasticSearch doesn't provide data consistency
- ElasticSearch doesn't provide a robust scheme 
  change without a bulk right after

# Proposal

## Using ElasticSearch as READ-ONLY

The idea is the following: the API writes into 
CQL, a bulk script is executed every X seconds, 
and the API reads ES for READ queries.

A problem shows up when using this solution: 
if the user creates a folder and tries to enter it, 
then an error will be thrown because the bulk script 
wouldn't have happened in time.

## syncing ES and CQL

In order to sync CQL and ES, we'll add Redis as a 
consistency monitor:

Write
1. `CQL: insert toto=1`
2. `Redis: set toto=1`

...the bulk script was executed yet:

READ
1. `Redis: get toto => success
   => READ from CQL`

...the bulk script was executed, and deleted 
   the files that got indexed in Redis:

READ
1. `Redis: get toto => doesn't exist
   => get from ES`

Conclusion
----------

At first, when a WRITE occurs, we insert the 
metadata into Redis, which we'll use in order 
to determinate whether it got indexed or not. 
Once the bulk occurs (ES indexation), we discard 
the metadata. When the metadata is present into 
Redis, the API will fetch the data from Scylla, 
otherwise it will fetch from ElasticSearch because 
it means that it got indexed. Inconsistency cannot 
happen due to the implicit monitoring of Redis from 
the API checks before executing READ queries.

For any modification made on a file and folder, the 
uuid of the file will have to be set in to Redis in 
order to determinate whether the data from ElasticSearch 
is deprecated or still valid.

## Disabling ES

We need to be able to disable ES temporarily for all the 
web servers in the case a crash or any incident happened 
within the ES cluster:

READ
1. `get ES => success => get toto => get CQL`

In the case one of the get from Redis isn't sucessful, the 
API will read from CQL straight away.

Conclusion
----------

Disabling ElasticSearch for all the web servers at once will 
only require one SET request into Redis.